import os
import pickle
import re
import time
from typing import Generator

import html2text
import requests
import tqdm
from bs4 import BeautifulSoup
from langchain.chains.qa_with_sources import load_qa_with_sources_chain
from langchain.docstore.document import Document
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.llms import OpenAI
from langchain.vectorstores.faiss import FAISS

r = requests.get("https://net.jogtar.hu/jogszabaly?docid=A1300005.TV")
soup = BeautifulSoup(r.content, "html.parser")
polgarjog = soup.find(id="results")
polgarjog = [html2text.html2text(content.text) for content in polgarjog.contents if content != "\n"]

chunked_polgarjog = [""]
for pj in polgarjog:
    pj = pj.replace("\n\n", "\n").replace("*", "")
    if "§" in pj:
        chunked_polgarjog.append(pj)
    else:
        chunked_polgarjog[-1] += pj

sources = []
contents = []
for pj in chunked_polgarjog:
    reg = re.search(".*§.*]\n", pj)
    if reg:
        if not pj[reg.span()[1] :]:
            continue
        sources.append(pj[reg.span()[0] : reg.span()[1]].replace("\n", ""))
        contents.append(pj[reg.span()[1] :])

source_chunks = [Document(page_content=c, metadata={"source": s}) for s, c in zip(sources, contents)]

texts = source_chunks

if not os.path.exists("search_index.pickle"):

    def chunks(lst: list[Document], n: int) -> Generator[list[Document], float, Document]:
        # https://stackoverflow.com/a/312464/18903720
        """Yield successive n-sized chunks from lst."""
        for i in range(0, len(lst), n):
            yield lst[i : i + n]

    embeddings = OpenAIEmbeddings()
    text_chunks = chunks(texts, 20)  # adjust 20 based on your average character count per line
    docsearch = FAISS.from_documents(texts[0:1], embeddings)
    for index, chunk in tqdm.tqdm(enumerate(text_chunks)):
        if index == 0:
            docsearch = FAISS.from_documents(chunk, embeddings)
        else:
            time.sleep(60)  # wait for a minute to not exceed any rate limits
            texts = [d.page_content for d in chunk]
            metadatas = [d.metadata for d in chunk]
            docsearch.add_texts(texts, metadatas)

    time.sleep(60)

    # search index to find the most relevant documents
    # docsearch = FAISS.from_documents(source_chunks, OpenAIEmbeddings())

    with open("search_index.pickle", "wb") as fw:
        pickle.dump(docsearch, fw)

else:
    with open("search_index.pickle", "rb") as fr:
        docsearch = pickle.load(fr)

# chain for the qa problem
chain = load_qa_with_sources_chain(OpenAI(temperature=0))


def print_answer(question: str) -> None:
    best_sources = docsearch.similarity_search(question, k=8)
    output_text = chain(
        {"input_documents": best_sources, "question": question},
        return_only_outputs=False,
    )

    print(output_text["output_text"])


print_answer("Milyen követelményt támaszt a jogalanyokkal szemben a joggal való visszaélés tilalma?")
print_answer("Mi az alapvető különbség a jóhiszemű és a rosszhiszemű magatartás között?")
print_answer("Világítsa meg a norma agendi és a facultas agendi közötti összefüggést!")
