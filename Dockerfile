FROM ubuntu:20.04
MAINTAINER gregiberri
ENV DEBIAN_FRONTEND=noninteractive
ENV SHELL=/bin/bash
ENV MAMBA_NO_BANNER=true
ENV LANG C.UTF-8
ENV LANGUAGE C.UTF-8
ENV LC_ALL C.UTF-8

RUN apt update
RUN apt install -y build-essential software-properties-common openssh-server git nano micro wget curl tmux screen htop

# Install Mamba(forge) and project dependencies
RUN curl -L -O https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-$(uname)-$(uname -m).sh
RUN bash Mambaforge-$(uname)-$(uname -m).sh -p /opt/conda -b
ENV MAMBA_ROOT_PREFIX="/opt/conda"
ENV PATH "$MAMBA_ROOT_PREFIX/bin:$PATH"
ADD environment.yml .
RUN mamba init bash > /dev/null
RUN mamba env create -f environment.yml
RUN echo "mamba activate law_gpt" >> ~/.bashrc

# add the code
WORKDIR /code
ADD . .

# ssh daemon
RUN mkdir -m 600 /root/.ssh
COPY authorized_keys /root/.ssh/authorized_keys
RUN chown root:root /root/.ssh/authorized_keys && chmod 600 /root/.ssh/authorized_keys
RUN sed -i s/#PermitRootLogin.*/PermitRootLogin\ yes/ /etc/ssh/sshd_config
RUN sed -i s/#PermitUserEnvironment.*/PermitUserEnvironment\ yes/ /etc/ssh/sshd_config
COPY entrypoint.sh /entrypoint.sh
RUN chmod 744 /entrypoint.sh

ENTRYPOINT /entrypoint.sh
